package carmanager;

public abstract class Car implements Servise {

    private final Model model;
    private final int yearOfProduction;
    private final int price;
    private final int weight;
    private final Color color;
    private final NameDriver driver;
    protected int distance = 0;
    protected int distanceOnService = 0;

    public Car(Model model, int yearOfProduction, int price, int weight, Color color, NameDriver driver) {
        this.model = model;
        this.yearOfProduction = yearOfProduction;
        this.price = price;
        this.weight = weight;
        this.color = color;
        this.driver = driver;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model=" + model +
                ", yearOfProduction=" + yearOfProduction +
                ", price=" + price +
                ", weight=" + weight +
                ", color=" + color +
                ", driver=" + driver +
                ", distance=" + distance +
                ", distanceOnService=" + distanceOnService +
                '}';
    }

}

