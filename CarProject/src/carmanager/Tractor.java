package carmanager;

public class Tractor implements Servise{

    private final String model;
    private int distance = 0;
    private int distanceOnService = 0;
    private final NameDriver driver;
    private final Color color;

    public Tractor(String model, NameDriver driver, Color color) {
        this.model = model;
        this.driver = driver;
        this.color = color;
    }

    public int getDistance() {
        return distance;
    }

    @Override
    public boolean isReadyToServise() {
        if (distanceOnService > 50000) {
            return true;
        } else {
            return false;
        }
    }



    @Override
    public void addDistance(int additinalDistance, int distance) {
        this.distance = this.distance + additinalDistance;
        distanceOnService = distanceOnService + additinalDistance;

    }

    @Override
    public String toString() {
        return "Tractor{" +
                "model='" + model + '\'' +
                ", distance=" + distance +
                ", distanceOnService=" + distanceOnService +
                ", driver=" + driver +
                ", color=" + color +
                '}';
    }
}

