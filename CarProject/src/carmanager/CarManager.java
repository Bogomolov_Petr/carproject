package carmanager;

import java.util.ArrayList;
import java.util.List;

public class CarManager {
    public static void main(String[] args) {
        
        PassengerCar toyota = new PassengerCar(Model.TOYOTA_PRIUS, 2008,15000,1200, Color.GREEN, NameDriver.PETR);
        FamilyCar renaut = new FamilyCar(Model.RENAUT_LOGAN, 2000, 9700, 1600, Color.BLACK, NameDriver.DIMA);
        Tractor belarus = new Tractor("AgroProm 12",NameDriver.MAX,Color.RED);

        List<Car> cars = new ArrayList<>();
        cars.add(toyota);
        cars.add(renaut);

        List<Tractor>tractors = new ArrayList<>();
        tractors.add(belarus);

        toyota.addDistance(20000,toyota.distance);
        renaut.addDistance(15000,renaut.distance);
        belarus.addDistance(50001,belarus.getDistance());

        System.out.println(toyota);
        System.out.println(renaut);
        System.out.println(belarus);

        System.out.println(toyota.isReadyToServise());
        System.out.println(renaut.isReadyToServise());
        System.out.println(belarus.isReadyToServise());

    }
}
